#include "window_main.h"
#include "ui_window_main.h"
#include "rep_transportmodule_replica.h"
#include "modulecontroller.h"
#include "sim_packages.h"

/**************************************************************************************************/
WindowMain::WindowMain( QSharedPointer<ModuleController> a_moduleController,
                        QSharedPointer<SimPackages> a_simPackages )
    : QMainWindow( nullptr )
    , ui( new Ui::WindowMain )
    , m_moduleController( a_moduleController )
    , m_simPackages( a_simPackages )
{
    ui->setupUi( this );

    ui->widGraph->setModuleController( m_moduleController );
    ui->widGraph->setSimPackages( m_simPackages );

    ui->tm1->setTransportModuleReplica( m_moduleController->getReplica( 0 ), 1 );
    ui->tm2->setTransportModuleReplica( m_moduleController->getReplica( 1 ), 2 );
    ui->tm3->setTransportModuleReplica( m_moduleController->getReplica( 2 ), 3 );
    ui->tm4->setTransportModuleReplica( m_moduleController->getReplica( 3 ), 4 );
    ui->tm5->setTransportModuleReplica( m_moduleController->getReplica( 4 ), 5 );

    // Feed in
    QObject::connect( ui->pbPkg30, &QPushButton::clicked,
                      m_simPackages.data(), &SimPackages::feed30cm_slot );

    QObject::connect( ui->pbPkg60, &QPushButton::clicked,
                      m_simPackages.data(), &SimPackages::feed60cm_slot );

    // General

    QObject::connect( ui->pbStart, &QPushButton::clicked,
                      m_moduleController.data(), &ModuleController::start );

    QObject::connect( ui->pbStartAll, &QPushButton::clicked,
                      m_moduleController.data(), &ModuleController::startAll );

    QObject::connect( ui->pbStopAll, &QPushButton::clicked,
                      m_moduleController.data(), &ModuleController::stopAll );

    QObject::connect( ui->pbFeedOutRelease, &QPushButton::clicked,
                      m_moduleController.data(), &ModuleController::releaseOne );

    QObject::connect(m_moduleController.data(), &ModuleController::controllerChanged,
                     ui->widGraph, &WidgetGraph::update);
}

/**************************************************************************************************/
WindowMain::~WindowMain()
{
    delete ui;
}
