#pragma once
#include <QWidget>

#include "rep_transportmodule_replica.h"

namespace Ui
{
class WidgetTransportModule;
}

class WidgetTransportModule : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetTransportModule( QWidget *parent );
    ~WidgetTransportModule();

    void setTransportModuleReplica( QSharedPointer<TransportModuleReplica> transportmodule,
                                    int module_nr);

public slots:
    void stateChanged_slot(TransportModuleReplica::State state, TransportModuleReplica::State oldState);
    void beltMovingChanged_slot( bool beltMoving );
    void beltVelocityChanged_slot( double beltSpeed );
    void beltPositionChanged_slot( double beltPosition );
    void photoCellBlockedChanged_slot( bool photoCellBlocked );

private:
    Ui::WidgetTransportModule *ui;
    QTimer *m_timer;
    QSharedPointer<TransportModuleReplica> m_transportmodule;
    int m_module_nr;
};
