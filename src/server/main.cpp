#include <QApplication>
#include <QCommandLineParser>
#include <QRemoteObjectHost>
#include <QSharedPointer>
#include <QHostInfo>
#include <cassert>
#include "window_main.h"
#include "transportmodule.h"
#include "../common/config.h"

/**************************************************************************************************/
int main( int argc, char *argv[] )
{
    QApplication app( argc, argv );
    app.setApplicationName( "Server" );
    app.setApplicationVersion( "0.0" );
    app.setWindowIcon( QIcon( ":/server.png" ) );


    // Handle command line
    QCommandLineParser parser;
    parser.setApplicationDescription( "RemoteObjectServer" );
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument( "modulenr",  "Module nr" );
    parser.process( app );

    const QStringList args = parser.positionalArguments();

    QString localhostname = QHostInfo::localHostName();

    int module_nr = 1;

    if( args.length() >= 1 )
    {
        bool ok;
        int nr = args[0].toInt( &ok );
        if( ok )
        {
            module_nr = nr;
        }
    }

    // Start remote object host
    QUrl remoteObjectAddress;
    remoteObjectAddress.setScheme( "tcp" );
    remoteObjectAddress.setHost( localhostname + ".local" );
    remoteObjectAddress.setPort( REMOTE_NODE_PORT + module_nr );
    QRemoteObjectHost srcNode( remoteObjectAddress,  REGISTRY_HOST_ADDR );

    // Start remote object
    QString remoteObjectName = REMOTE_OBJECT_NAME_PREFIX + QString::number( module_nr );
    QSharedPointer<TransportModule> transportmodule(
        new TransportModule( nullptr, QString( remoteObjectName ) ) );
    assert( srcNode.enableRemoting( transportmodule.data(), remoteObjectName ) );

    // Start GUI
    QSharedPointer<WindowMain> windowMain( new WindowMain( transportmodule ) );
    windowMain->setWindowTitle( remoteObjectName );
    windowMain->show();
    windowMain->move( 360 * ( module_nr - 1 ), 0 );

    return app.exec();
}
