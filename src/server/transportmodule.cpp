#include "transportmodule.h"
#include <QTimer>
#include <algorithm>

static const double SLOWMOTION = 1.0;
static const double BELT_LENGTH         /* m    */ = 0.750;
static const double PHOTOCELL_POSITION  /* m    */ = 0.650;
static const double MAX_VELOCITY        /* m/s  */ = 0.5 * SLOWMOTION;
static const double ACCELERATION        /* m/s2 */ = 4.0 * SLOWMOTION * SLOWMOTION;
static const double SAMPLE_RESOLUTION   /* m    */ = 0.010;
static const double SAMPLE_PERIOD       /* s    */ = SAMPLE_RESOLUTION / MAX_VELOCITY;
static const double STARTSTOP_DURATION  /* s    */ = MAX_VELOCITY / ACCELERATION;
static const double STARTSTOP_DISTANCE  /* m    */ = MAX_VELOCITY / 2.0 * STARTSTOP_DURATION;

static const bool SIMULATE = true;

/**************************************************************************************************/
TransportModule::TransportModule( QObject *parent, const QString remoteObjectName )
    : TransportModuleSimpleSource( parent )
    , m_remoteObjectName( remoteObjectName )
    , m_timer( new QTimer( this ) )
{
    setBeltLength( BELT_LENGTH );
    setBeltMaxStartStopDuration( STARTSTOP_DURATION );
    setBeltMaxStartStopDistance( STARTSTOP_DISTANCE );
    setBeltMaxVelocity( MAX_VELOCITY );
    setBeltVelocity( 0.0 );
    setBeltPosition( 0.0 );

    setPhotoCellPosition( PHOTOCELL_POSITION );
    setPhotoCellBlocked( false );

    setBeltMoving( false );

    m_integrateTimer.restart();
    QObject::connect( m_timer, &QTimer::timeout,
                      this, &TransportModule::timeout_slot );
    m_timer->start( SAMPLE_PERIOD * 1000 );
}

/**************************************************************************************************/
TransportModule::~TransportModule()
{
}

/**************************************************************************************************/
void TransportModule::timeout_slot()
{
    auto currentBeltVelocity = beltVelocity();

    update_velocity( currentBeltVelocity );
    update_positions( currentBeltVelocity );
}

/**************************************************************************************************/
void TransportModule::update_velocity( double currentBeltVelocity )
{
    if( !SIMULATE )
    {
        // TODO
    }
    else
    {
        if( beltMoving() )
        {
            auto dt = m_startTimer.elapsed( true );
            setBeltVelocity( std::min( currentBeltVelocity + ACCELERATION * dt, MAX_VELOCITY ) );
        }
        else
        {
            auto dt = m_stopTimer.elapsed( true );
            setBeltVelocity( std::max( currentBeltVelocity - ACCELERATION * dt, 0.0 ) );
        }
    }
}

/**************************************************************************************************/
void TransportModule::update_positions( double currentBeltVelocity )
{
    static double previousVelocity = 0.0;

    double dt = m_integrateTimer.elapsed( true );
    double avgVelocity = ( previousVelocity +  currentBeltVelocity ) / 2.0;
    previousVelocity = currentBeltVelocity;

    double dx = + avgVelocity * dt;

    setBeltPosition( beltPosition()  + dx );

    // update heads and sensors
    QList<double> heads = headPositions();
    for( int i = 0; i < heads.length(); ++i )
    {
        if( ( heads[i] < PHOTOCELL_POSITION ) &&
                ( heads[i] + dx >= PHOTOCELL_POSITION ) )
        {
            setPhotoCellBlocked( true );
        }
        heads[i] = heads[i] + dx;
    }
    for( int i = heads.length() - 1; i >= 0; --i )
    {
        if( heads[i] > BELT_LENGTH )
        {
            heads.removeAt( i );
            emit hintHeadOut();
        }
    }
    setHeadPositions( heads );

    // update tails
    QList<double> tails = tailPositions();
    for( int i = 0; i < tails.length(); ++i )
    {
        if( ( tails[i] < PHOTOCELL_POSITION ) &&
                ( tails[i] + dx >= PHOTOCELL_POSITION ) )
        {
            setPhotoCellBlocked( false );
        }
        tails[i] = tails[i] + dx;
    }
    for( int i = tails.length() - 1; i >= 0; --i )
    {
        if( tails[i] > BELT_LENGTH )
        {
            tails.removeAt( i );
            emit hintTailOut();
        }
    }
    setTailPositions( tails );
}

/**************************************************************************************************/
void TransportModule::startBelt()
{
    setBeltMoving( true );
    if( SIMULATE )
    {
        m_startTimer.restart();
    }
}

/**************************************************************************************************/
void TransportModule::stopBelt()
{
    setBeltMoving( false );
    if( SIMULATE )
    {
        m_stopTimer.restart();
    }
}

/**************************************************************************************************/
void TransportModule::hintHeadIn()
{
    QList<double> heads = headPositions();
    heads.prepend( 0.0 );
    setHeadPositions( heads );
}

/**************************************************************************************************/
void TransportModule::hintTailIn()
{
    QList<double> tails = tailPositions();
    tails.prepend( 0.0 );
    setTailPositions( tails );
}
