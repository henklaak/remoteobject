#include "timer.h"
#include <thread>

/**************************************************************************************************/
Timer::Timer()
{
    m_start = std::chrono::high_resolution_clock::now();
}

/**************************************************************************************************/
void Timer::restart()
{
    m_start = std::chrono::high_resolution_clock::now();
}

/**************************************************************************************************/
double Timer::elapsed( bool restart )
{
    auto dt = elapsed_us(restart)/1000000.0;
    return dt;
}

/**************************************************************************************************/
long long Timer::elapsed_s( bool restart )
{
    auto now  = std::chrono::high_resolution_clock::now();
    auto elapsed = now - m_start;
    if( restart )
    {
        m_start = now;
    }
    long long s = std::chrono::duration_cast<std::chrono::seconds>( elapsed ).count();
    return s;
}

/**************************************************************************************************/
long long Timer::elapsed_ms( bool restart )
{
    auto now  = std::chrono::high_resolution_clock::now();
    auto elapsed = now - m_start;
    if( restart )
    {
        m_start = now;
    }
    long long ms = std::chrono::duration_cast<std::chrono::milliseconds>( elapsed ).count();
    return ms;
}

/**************************************************************************************************/
long long Timer::elapsed_us( bool restart )
{
    auto now  = std::chrono::high_resolution_clock::now();
    auto elapsed = now - m_start;
    if( restart )
    {
        m_start = now;
    }
    long long us = std::chrono::duration_cast<std::chrono::microseconds>( elapsed ).count();
    return us;
}

/**************************************************************************************************/
void Timer::sleep_s( unsigned long s )
{
    std::this_thread::sleep_for( std::chrono::seconds( s ) );
}

/**************************************************************************************************/
void Timer::sleep_ms( unsigned long ms )
{
    std::this_thread::sleep_for( std::chrono::milliseconds( ms ) );
}

/**************************************************************************************************/
void Timer::sleep_us( unsigned long us )
{
    std::this_thread::sleep_for( std::chrono::microseconds( us ) );
}


