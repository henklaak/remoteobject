#include <QApplication>
#include <rep_transportmodule_replica.h>

#include "window_main.h"
#include "modulecontroller.h"
#include "sim_packages.h"
#include "../common/config.h"

const int NRNODES = 5;

/**************************************************************************************************/
int main( int argc, char *argv[] )
{
    QApplication app( argc, argv );
    app.setApplicationName( "Client" );
    app.setApplicationVersion( "0.0" );
    app.setWindowIcon( QIcon( ":/client.png" ) );

    QRemoteObjectNode registryNode( REGISTRY_HOST_ADDR );
    QList<QSharedPointer<TransportModuleReplica>> ptrs;

    for( size_t n = 1; n <= NRNODES; ++n )
    {
        QString remoteObjectName = REMOTE_OBJECT_NAME_PREFIX + QString::number( n );

        QSharedPointer<TransportModuleReplica> ptr;
        ptr.reset( registryNode.acquire<TransportModuleReplica>( remoteObjectName ) );
        ptr->waitForSource();
        ptrs.append( ptr );
    }

    QSharedPointer<ModuleController> moduleController( new ModuleController( ptrs ) );
    QSharedPointer<SimPackages> simPackages( new SimPackages( moduleController ) );

    QObject::connect( simPackages.data(), &SimPackages::hintHeadIn,
                      moduleController->getReplica( 0 ).data(), &TransportModuleReplica::hintHeadIn );
    QObject::connect( simPackages.data(), &SimPackages::hintTailIn,
                      moduleController->getReplica( 0 ) .data(), &TransportModuleReplica::hintTailIn );

    QSharedPointer<WindowMain> windowMain( new WindowMain( moduleController, simPackages ) );
    windowMain->setWindowTitle( "Client" );
    windowMain->move( 2560 + 360, 360 );
    windowMain->show();

    return app.exec();
}
