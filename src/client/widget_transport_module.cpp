#include "widget_transport_module.h"
#include "ui_widget_transport_module.h"
#include "rep_transportmodule_replica.h"

/**************************************************************************************************/
WidgetTransportModule::WidgetTransportModule( QWidget *parent )
    : QWidget( parent )
    , ui( new Ui::WidgetTransportModule )
    , m_module_nr( 0 )
{
    ui->setupUi( this );

}

/**************************************************************************************************/
WidgetTransportModule::~WidgetTransportModule()
{
    delete ui;
}

/**************************************************************************************************/
void WidgetTransportModule::setTransportModuleReplica(
    QSharedPointer<TransportModuleReplica> a_transportmodule,
    int a_module_nr )
{
    m_transportmodule = a_transportmodule;
    m_module_nr = a_module_nr;

    QObject::connect( m_transportmodule.data(), &TransportModuleReplica::stateChanged,
                      this, &WidgetTransportModule::stateChanged_slot );

    QObject::connect( m_transportmodule.data(), &TransportModuleReplica::beltMovingChanged,
                      this, &WidgetTransportModule::beltMovingChanged_slot );

    QObject::connect( m_transportmodule.data(), &TransportModuleReplica::beltVelocityChanged,
                      this, &WidgetTransportModule::beltVelocityChanged_slot );

    QObject::connect( m_transportmodule.data(), &TransportModuleReplica::beltPositionChanged,
                      this, &WidgetTransportModule::beltPositionChanged_slot );

    QObject::connect( m_transportmodule.data(), &TransportModuleReplica::photoCellBlockedChanged,
                      this, &WidgetTransportModule::photoCellBlockedChanged_slot );

    /* Init */
    bool valid = m_transportmodule->state() == TransportModuleReplica::State::Valid;
    ui->lblAvailable->setText( "#" + QString::number( m_module_nr ) + ( valid ? " online" :
                               " offline" ) );
    ui->lblAvailable->setStyleSheet( valid ? "color: #4f4;" : "color: #f44;" );
    ui->lblBeltMoving->setText( m_transportmodule->beltMoving() ? "Yes" : "No" );
    ui->lblBeltVelocity->setText( QString::number( m_transportmodule->beltVelocity(), 'f', 2 ) );
    ui->lblBeltPosition->setText( QString::number( m_transportmodule->beltPosition(), 'f', 2 ) );
    ui->lblPhotoCellBlocked->setText( m_transportmodule->photoCellBlocked() ? "Yes" : "No" );

}

/**************************************************************************************************/
void WidgetTransportModule::stateChanged_slot(
    TransportModuleReplica::State state,
    TransportModuleReplica::State /*oldState*/ )
{
    switch( state )
    {
    case TransportModuleReplica::State::Valid:
        ui->lblAvailable->setText( "#" + QString::number( m_module_nr ) + " online" );
        ui->lblAvailable->setStyleSheet( "color: #484;" );
        break;
    default:
        ui->lblAvailable->setText( "#" + QString::number( m_module_nr ) + " offline" );
        ui->lblAvailable->setStyleSheet( "color: #f44;" );
        break;
    }
}

/**************************************************************************************************/
void WidgetTransportModule::beltMovingChanged_slot( bool beltMoving )
{
    ui->lblBeltMoving->setText( beltMoving ? "Yes" : "No" );
}

/**************************************************************************************************/
void WidgetTransportModule::beltVelocityChanged_slot( double beltVelocity )
{
    ui->lblBeltVelocity->setText( QString::number( beltVelocity, 'f', 2 ) );
}

/**************************************************************************************************/
void WidgetTransportModule::beltPositionChanged_slot( double beltVelocity )
{
    ui->lblBeltPosition->setText( QString::number( beltVelocity, 'f', 3 ) );
}

/**************************************************************************************************/
void WidgetTransportModule::photoCellBlockedChanged_slot( bool photoCellBlocked )
{
    ui->lblPhotoCellBlocked->setText( photoCellBlocked ? "Yes" : "No" );
}
