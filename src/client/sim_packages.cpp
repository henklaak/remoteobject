#include "sim_packages.h"
#include <QDebug>
#include "modulecontroller.h"
#include "rep_transportmodule_replica.h"

/**************************************************************************************************/
SimPackages::SimPackages( QSharedPointer<ModuleController> a_moduleController )
    : QObject( nullptr )
    , m_moduleController( a_moduleController )
{
    headTimer.setSingleShot( true );
    tailTimer.setSingleShot( true );
    QObject::connect( &headTimer, &QTimer::timeout, this, &SimPackages::hintHeadIn );
    QObject::connect( &tailTimer, &QTimer::timeout, this, &SimPackages::hintTailIn );
}

/**************************************************************************************************/
void SimPackages::feed30cm_slot()
{
    if( !tailTimer.isActive() )
    {
        headTimer.start( 10 );
        tailTimer.start( 10 + 300 / m_moduleController->getReplica( 0 )->beltMaxVelocity() );
    }
}

/**************************************************************************************************/
void SimPackages::feed60cm_slot()
{
    if( !tailTimer.isActive() )
    {
        headTimer.start( 10 );
        tailTimer.start( 10 + 600 / m_moduleController->getReplica( 0 )->beltMaxVelocity() );
    }
}
