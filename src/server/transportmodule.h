#pragma once

#include <QObject>
#include "rep_transportmodule_source.h"
#include "timer.h"

class QTimer;

class TransportModule : public TransportModuleSimpleSource
{
    Q_OBJECT

public:
    explicit TransportModule( QObject *parent, const QString remoteObjectName );
    ~TransportModule();

    void startBelt() override;
    void stopBelt() override;
    void hintHeadIn() override;
    void hintTailIn() override;

private slots:
    void timeout_slot();

private:
    void update_velocity( double currentBeltVelocity );
    void update_positions( double currentBeltVelocity );

    QString m_remoteObjectName;
    QTimer *m_timer;
    Timer m_integrateTimer;
    Timer m_startTimer;
    Timer m_stopTimer;
};
