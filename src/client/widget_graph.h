#pragma once
#include <QWidget>
#include <QQueue>
#include <QPen>
#include "rep_transportmodule_replica.h"

class ModuleController;
class SimPackages;

class WidgetGraph : public QWidget
{
    Q_OBJECT
public:
    explicit WidgetGraph( QWidget *parent = nullptr );

    void setModuleController( QSharedPointer<ModuleController> a_moduleController );
    void setSimPackages( QSharedPointer<SimPackages> a_simPackages );

    void paintEvent( QPaintEvent *event ) override;

public slots:
    void update()
    {
        QWidget::update();
    };

private:
    void paintBelt( QPainter &painter, int module_index );
    void paintPackages( QPainter &painter, int module_index );
    void paintFeedIn( QPainter &painter );
    void paintFeedOut( QPainter &painter );

    QSharedPointer<ModuleController> m_moduleController;
    QSharedPointer<SimPackages> m_simPackages;

    QPen m_green, m_black, m_red;
};

