#include "modulecontroller.h"
#include "rep_transportmodule_replica.h"
#include <QTimer>


/**************************************************************************************************/
ModuleController::ModuleController(
    QList<QSharedPointer<TransportModuleReplica>> a_transportmodulereplicas )
    : QObject( nullptr )
    , m_transportmodulereplicas( a_transportmodulereplicas )
    , m_accepting( true )
    , m_releasing( false )
{
    // Connect events
    for( int i = 0; i < NR_MODULES; ++i )
    {
        QObject::connect( m_transportmodulereplicas[i].data(), &TransportModuleReplica::hintHeadOut,
                          [ = ]()
        {
            hintHeadOut_slot( i );
        } );
        QObject::connect( m_transportmodulereplicas[i].data(), &TransportModuleReplica::hintTailOut,
                          [ = ]()
        {
            hintTailOut_slot( i );
        } );
        QObject::connect( m_transportmodulereplicas[i].data(),
                          &TransportModuleReplica::photoCellBlockedChanged,
                          [ = ]( bool photoCellBlocked )
        {
            photoCellBlocked_slot( i, photoCellBlocked );
        } );
    }
}

/**************************************************************************************************/
QSharedPointer<TransportModuleReplica> ModuleController::getReplica( int index )
{
    return m_transportmodulereplicas[index];
}


/**************************************************************************************************/
int ModuleController::nrHeadsInModule( int module_index )
{
    return m_transportmodulereplicas[module_index]->headPositions().length();
}

/**************************************************************************************************/
void ModuleController::start()
{
    m_beltMode = BELTMODE_ACCUMULATE;
    m_releasing = false;
    m_transportmodulereplicas[0]->startBelt();
}


/**************************************************************************************************/
void ModuleController::startAll()
{
    m_beltMode = BELTMODE_CONTINUOUS;
    m_releasing = true;
    for( int i = 0; i < NR_MODULES; ++i )
    {
        m_transportmodulereplicas[i]->startBelt();
    }
}

/**************************************************************************************************/
void ModuleController::stopAll()
{
    m_beltMode = BELTMODE_STOPALL;
    m_releasing = false;
    for( int i = 0; i < NR_MODULES; ++i )
    {
        m_transportmodulereplicas[i]->stopBelt();
    }
}

/**************************************************************************************************/
void ModuleController::releaseOne()
{
    if( m_beltMode == BELTMODE_ACCUMULATE )
    {
        if( nrHeadsInModule( NR_MODULES - 1 ) & !m_transportmodulereplicas[NR_MODULES - 1]->beltMoving() )
        {
            if( nrHeadsInModule( 3 ) || nrHeadsInModule( 4 ) )
            {
                m_transportmodulereplicas[4]->startBelt();
            }
            if( nrHeadsInModule( 2 ) || nrHeadsInModule( 3 ) )
            {
                m_transportmodulereplicas[3]->startBelt();
            }
            if( nrHeadsInModule( 1 ) || nrHeadsInModule( 2 ) )
            {
                m_transportmodulereplicas[2]->startBelt();
            }
            if( nrHeadsInModule( 0 ) || nrHeadsInModule( 1 ) )
            {
                m_transportmodulereplicas[1]->startBelt();
            }
            if( nrHeadsInModule( 0 ) )
            {
                m_transportmodulereplicas[0]->startBelt();
            }
        }
    }
}

/**************************************************************************************************/
/**************************************************************************************************/
void ModuleController::hintHeadOut_slot( int module_index )
{
    // Verify that accepting belt is moving
    if( module_index + 1 <  NR_MODULES )
    {
        assert( m_transportmodulereplicas[module_index + 1]->beltMoving() );
        m_transportmodulereplicas[module_index + 1]->hintHeadIn();
    }
}

/**************************************************************************************************/
void ModuleController::hintTailOut_slot( int module_index )
{
    // Verify that accepting belt is moving
    if( module_index + 1 <  NR_MODULES )
    {
        assert( m_transportmodulereplicas[module_index + 1]->beltMoving() );
        m_transportmodulereplicas[module_index + 1]->hintTailIn();
    }

    if( m_beltMode == BELTMODE_ACCUMULATE )
    {
        switch( module_index )
        {
        case 1:
            if( nrHeadsInModule( 0 ) + nrHeadsInModule( 1 ) == 0 )
            {
                m_transportmodulereplicas[module_index]->stopBelt();
            }
            break;
        case 2:
            if( nrHeadsInModule( 1 ) + nrHeadsInModule( 2 ) == 0 )
            {
                m_transportmodulereplicas[module_index]->stopBelt();
            }
            break;
        case 3:
            if( nrHeadsInModule( 2 ) + nrHeadsInModule( 3 ) == 0 )
            {
                m_transportmodulereplicas[module_index]->stopBelt();
            }
            break;
        case 4:
            if( nrHeadsInModule( 3 ) + nrHeadsInModule( 4 ) == 0 )
            {
                m_transportmodulereplicas[module_index]->stopBelt();
            }
            break;
        }
    }
}

/**************************************************************************************************/
void ModuleController::photoCellBlocked_slot( int module_index, bool photoCellBlocked )
{
    if( m_beltMode == BELTMODE_ACCUMULATE )
    {

        if( photoCellBlocked )
        {
            //Decide to pass or hold
            switch( module_index )
            {
            case 0:
            {
                int totheright = nrHeadsInModule( 1 ) + nrHeadsInModule( 2 ) + nrHeadsInModule(
                                     3 ) + nrHeadsInModule( 4 );
                if( totheright >= 4 )
                {
                    m_transportmodulereplicas[module_index]->stopBelt();
                }
                else
                {
                    m_transportmodulereplicas[module_index + 1]->startBelt();
                }
                break;
            }
            case 1:
            {
                int totheright = nrHeadsInModule( 2 ) + nrHeadsInModule( 3 ) + nrHeadsInModule( 4 );
                if( totheright >= 3 )
                {
                    m_transportmodulereplicas[module_index]->stopBelt();
                }
                else
                {
                    m_transportmodulereplicas[module_index + 1]->startBelt();
                }
                break;
            }
            case 2:
            {
                int totheright = nrHeadsInModule( 3 ) + nrHeadsInModule( 4 );
                if( totheright >= 2 )
                {
                    m_transportmodulereplicas[module_index]->stopBelt();
                }
                else
                {
                    m_transportmodulereplicas[module_index + 1]->startBelt();
                }
                break;
            }
            case 3:
            {
                int totheright = nrHeadsInModule( 4 );
                if( totheright >= 1 )
                {
                    m_transportmodulereplicas[module_index]->stopBelt();
                }
                else
                {
                    m_transportmodulereplicas[module_index + 1]->startBelt();
                }
                break;
            }
            case 4:
            {
                if( !m_releasing )
                {
                    m_transportmodulereplicas[module_index]->stopBelt();
                }
                break;
            }

            }
        }
    }
}

