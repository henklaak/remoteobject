#pragma once
#include <QMainWindow>

class ModuleController;
class SimPackages;

namespace Ui
{
class WindowMain;
}

class WindowMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit WindowMain( QSharedPointer<ModuleController> a_moduleController,
                         QSharedPointer<SimPackages> m_simPackages );
    ~WindowMain();

private:
    Ui::WindowMain *ui;
    QSharedPointer<ModuleController> m_moduleController;
    QSharedPointer<SimPackages> m_simPackages;
};
