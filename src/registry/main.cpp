#include <QApplication>
#include <QRemoteObjectRegistryHost>
#include <QDebug>
#include "../common/config.h"

/**************************************************************************************************/
int main( int argc, char *argv[] )
{
    QApplication app( argc, argv );
    app.setApplicationName( "Registry" );
    app.setApplicationVersion( "0.0" );

    QRemoteObjectRegistryHost regNode( REGISTRY_HOST_ADDR );

    return app.exec();
}
