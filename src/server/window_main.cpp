#include "window_main.h"
#include "ui_window_main.h"
#include "transportmodule.h"

/**************************************************************************************************/
WindowMain::WindowMain( QSharedPointer<TransportModule> transportmodule )
    : QMainWindow( nullptr )
    , ui( new Ui::WindowMain )
    , m_transportmodule( transportmodule )
{
    ui->setupUi( this );

    QObject::connect( m_transportmodule.data(), &TransportModule::beltMovingChanged,
                      this, &WindowMain::beltMovingChanged_slot );
    QObject::connect( m_transportmodule.data(), &TransportModule::beltVelocityChanged,
                      this, &WindowMain::beltVelocityChanged_slot );
    QObject::connect( m_transportmodule.data(), &TransportModule::beltPositionChanged,
                      this, &WindowMain::beltPositionChanged_slot );
    QObject::connect( m_transportmodule.data(), &TransportModule::photoCellBlockedChanged,
                      this, &WindowMain::photoCellBlockedChanged_slot );

    /* Init */
    ui->lblBeltMoving->setText( m_transportmodule->beltMoving() ? "Yes" : "No" );
    ui->lblBeltVelocity->setText( QString::number( m_transportmodule->beltVelocity(), 'f', 2 ) );
    ui->lblBeltPosition->setText( QString::number( m_transportmodule->beltPosition(), 'f', 3 ) );
    ui->lblPhotoCellBlocked->setText( m_transportmodule->photoCellBlocked() ? "Yes" : "No" );
}

/**************************************************************************************************/
WindowMain::~WindowMain()
{
    delete ui;
}

/**************************************************************************************************/
void WindowMain::beltMovingChanged_slot( bool beltMoving )
{
    ui->lblBeltMoving->setText( beltMoving ? "Yes" : "No" );
}

/**************************************************************************************************/
void WindowMain::beltVelocityChanged_slot( double beltVelocity )
{
    ui->lblBeltVelocity->setText( QString::number( beltVelocity, 'f', 2 ) );
}

/**************************************************************************************************/
void WindowMain::beltPositionChanged_slot( double beltPosition )
{
    ui->lblBeltPosition->setText( QString::number( beltPosition, 'f', 3 ) );
}

/**************************************************************************************************/
void WindowMain::photoCellBlockedChanged_slot( bool photoCellBlocked )
{
    ui->lblPhotoCellBlocked->setText( photoCellBlocked ? "Yes" : "No" );
}
