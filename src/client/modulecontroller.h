#pragma once
#include <QObject>
#include <QSharedPointer>
#include <QList>

#define NR_MODULES (5)

class TransportModuleReplica;

class ModuleController : public QObject
{
    Q_OBJECT
public:
    explicit ModuleController(
        QList<QSharedPointer<TransportModuleReplica>> a_transportmodulereplicas );

    QSharedPointer<TransportModuleReplica> getReplica( int a );

    int nrHeadsInModule( int module_index );

public slots:
    void start();
    void startAll();
    void stopAll();
    void releaseOne();

signals:
    void controllerChanged();

private slots:
    void hintHeadOut_slot( int module_index );
    void hintTailOut_slot( int module_index );
    void photoCellBlocked_slot( int module_index, bool photoCellBlocked );

private:
    enum BeltMode
    {
        BELTMODE_STOPALL = 0,
        BELTMODE_CONTINUOUS,
        BELTMODE_ACCUMULATE,
    };

    QList<QSharedPointer<TransportModuleReplica>> m_transportmodulereplicas;

    BeltMode m_beltMode;

    bool m_accepting;
    bool m_releasing;
};
