#pragma once

#include <QObject>
#include <QSharedPointer>
#include <QTimer>

class ModuleController;

class SimPackages : public QObject
{
    Q_OBJECT
public:
    explicit SimPackages(QSharedPointer<ModuleController> a_moduleController);

signals:
    void hintHeadIn();
    void hintTailIn();

public slots:
    void feed30cm_slot();
    void feed60cm_slot();

private:
    QSharedPointer<ModuleController> m_moduleController;

    QTimer headTimer;
    QTimer tailTimer;
};
