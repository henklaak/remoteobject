#include "widget_graph.h"
#include <algorithm>
#include <math.h>
#include <QPainter>
#include "modulecontroller.h"
#include "sim_packages.h"

const double RADIUS = 0.05;
const double DIAMETER = RADIUS * 2;

/**************************************************************************************************/
WidgetGraph::WidgetGraph( QWidget *parent )
    : QWidget{parent}
{
    setAttribute( Qt::WA_TranslucentBackground );

    m_green = QPen( QColor::fromRgb( 0, 192, 0 ) );
    m_green.setCosmetic( true );
    m_green.setWidth( 1 );

    m_red = QPen( QColor::fromRgb( 192, 0, 0 ) );
    m_red.setCosmetic( true );
    m_red.setWidth( 1 );

    m_black = QPen( QColor::fromRgb( 0, 0, 0 ) );
    m_black.setCosmetic( true );
    m_black.setWidth( 1 );
}

/**************************************************************************************************/
void WidgetGraph::setModuleController( QSharedPointer<ModuleController> a_moduleController )
{
    m_moduleController = a_moduleController;
    for( int i = 0; i < 5; ++i )
    {
        QObject::connect( m_moduleController->getReplica( i ).data(),
                          &TransportModuleReplica::beltPositionChanged,
                          [this]()
        {
            update();
        } );
    }
}

/**************************************************************************************************/
void WidgetGraph::setSimPackages( QSharedPointer<SimPackages> a_simPackages )
{
    m_simPackages = a_simPackages;
}

/**************************************************************************************************/
void WidgetGraph::paintEvent( QPaintEvent *event )
{
    QPainter painter( this );
    painter.setRenderHint( QPainter::TextAntialiasing );
    painter.setRenderHint( QPainter::Antialiasing );


    double beltLength = m_moduleController->getReplica( 0 )->beltLength();

    QTransform trafo;
    trafo.translate( 0, 1 );
    trafo.scale( 1024 / ( 5 * beltLength + 2.5 ), -1024 / ( 5 * beltLength + 2.5 ) );
    trafo.translate( 1.25, -1.0 );
    painter.setTransform( trafo );

    paintFeedIn( painter );
    for( int i = 0; i < 5; ++i )
    {
        paintBelt( painter, i );
        paintPackages( painter, i );
    }
    paintFeedOut( painter );

}

/**************************************************************************************************/
void WidgetGraph::paintBelt( QPainter &painter, int module_index )
{
    double beltLength = m_moduleController->getReplica( module_index )->beltLength();
    double beltPosition = m_moduleController->getReplica( module_index )->beltPosition();

    double beltLocation = module_index * beltLength;

    // Draw position markers
    painter.setPen( m_black );
    for( int i = 0; i <= 10; ++i )
    {
        double position = fmod( beltPosition + i / 10.0 * beltLength, beltLength );
        if( position > RADIUS / 2 && position < beltLength - RADIUS / 2 )
        {
            painter.drawLine( QLineF( beltLocation + position, RADIUS,
                                      beltLocation + position, -RADIUS ) );
        }
    }

    // Draw belt
    painter.setPen( m_black );
    {
        QRectF rc( beltLocation, -RADIUS,
                   DIAMETER, DIAMETER );
        painter.drawArc( rc, 90 * 16, 180 * 16 );
    }
    {
        QRectF rc( beltLocation + beltLength -  DIAMETER, -RADIUS,
                   DIAMETER, DIAMETER );
        painter.drawArc( rc, -90 * 16, 180 * 16 );
    }
    {
        painter.drawLine( QLineF( beltLocation +  RADIUS,       RADIUS,
                                  beltLocation + beltLength  - RADIUS, RADIUS ) );
        painter.drawLine( QLineF( beltLocation + RADIUS,        -RADIUS,
                                  beltLocation + beltLength - RADIUS,  -RADIUS ) );
    }

    // Draw semaphore
    painter.save();
    painter.scale( 0.01, -0.01 );
    painter.translate( 30 + 74 * module_index, 20.0 );
    painter.drawText( QPointF( beltLocation, -0.1 ),
                      QString::number( m_moduleController->nrHeadsInModule( module_index ) ) );
    painter.restore();
}

/**************************************************************************************************/
void WidgetGraph::paintPackages( QPainter &painter, int module_index )
{
    double beltLength = m_moduleController->getReplica( module_index )->beltLength();
    double beltLocation = module_index * beltLength;

    bool photoCellBlocked = m_moduleController->getReplica( module_index )->photoCellBlocked();
    double photoCellPosition = m_moduleController->getReplica( module_index )->photoCellPosition();
    painter.setPen( m_black );
    if( photoCellBlocked )
    {
        painter.drawRect( QRectF( QPointF( beltLocation + photoCellPosition - 0.01, 0.11 ),
                                  QPointF( beltLocation + photoCellPosition + 0.01, 0.09 ) ) );

    }

    QList<double> heads = m_moduleController->getReplica( module_index )->headPositions();
    QList<double> tails = m_moduleController->getReplica( module_index )->tailPositions();

    painter.setPen( m_red );
    for( int i = 0; i < heads.length(); ++i )
    {
        double dx = heads[i];

        painter.drawLine( QLineF( beltLocation + dx, 0.3,
                                  beltLocation + dx, RADIUS ) );

    }
    painter.setPen( m_green );
    for( int i = 0; i < tails.length(); ++i )
    {
        double dx = tails[i];

        painter.drawLine( QLineF( beltLocation + dx, 0.3,
                                  beltLocation + dx, RADIUS ) );

    }
}

/**************************************************************************************************/
void WidgetGraph::paintFeedIn( QPainter & painter )
{
    double beltLength = 1.25;
    double beltLocation = -1 * beltLength;

    painter.setPen( m_black );
    {
        QRectF rc( beltLocation + beltLength -  DIAMETER, -RADIUS,
                   DIAMETER, DIAMETER );
        painter.drawArc( rc, -90 * 16, 180 * 16 );
    }
    {
        painter.drawLine( QLineF( beltLocation +  RADIUS,       RADIUS,
                                  beltLocation + beltLength  - RADIUS, RADIUS ) );
        painter.drawLine( QLineF( beltLocation + RADIUS,        -RADIUS,
                                  beltLocation + beltLength - RADIUS,  -RADIUS ) );
    }

}

/**************************************************************************************************/
void WidgetGraph::paintFeedOut( QPainter & painter )
{
    double beltLength = 1.25;
    double beltLocation = 5 * m_moduleController->getReplica( 0 )->beltLength();

    painter.setPen( m_black );
    {
        QRectF rc( beltLocation, -RADIUS,
                   DIAMETER, DIAMETER );
        painter.drawArc( rc, 90 * 16, 180 * 16 );
    }
    {
        painter.drawLine( QLineF( beltLocation +  RADIUS,       RADIUS,
                                  beltLocation + beltLength  - RADIUS, RADIUS ) );
        painter.drawLine( QLineF( beltLocation + RADIUS,        -RADIUS,
                                  beltLocation + beltLength - RADIUS,  -RADIUS ) );
    }
}
