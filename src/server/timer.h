#ifndef TIMER_H
#define TIMER_H

#include <chrono>

class Timer
{
public:
    Timer();

    void restart();
    double elapsed( bool restart = false );
    long long elapsed_s( bool restart = false );
    long long elapsed_ms( bool restart = false );
    long long elapsed_us( bool restart = false );

    static void sleep_s( unsigned long );
    static void sleep_ms( unsigned long );
    static void sleep_us( unsigned long );

private:
    std::chrono::high_resolution_clock::time_point m_start;
};

#endif // TIMER_H
