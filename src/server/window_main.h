#pragma once

#include <QMainWindow>

class TransportModule;

namespace Ui {
class WindowMain;
}

class WindowMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit WindowMain(QSharedPointer<TransportModule> transportmodule);
    ~WindowMain();

public slots:
    void beltMovingChanged_slot( bool beltMoving );
    void beltVelocityChanged_slot(double beltVelocity);
    void beltPositionChanged_slot(double beltPosition);
    void photoCellBlockedChanged_slot( bool photoCellBlocked);

private:
    Ui::WindowMain *ui;
    QSharedPointer<TransportModule> m_transportmodule;
};
