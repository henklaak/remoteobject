#pragma once
#include <QUrl>

const QUrl REGISTRY_HOST_ADDR = QUrl(QStringLiteral( "tcp://192.168.178.10" ));
const int REMOTE_NODE_PORT = 65300;
const QString REMOTE_OBJECT_NAME_PREFIX = QStringLiteral("transportmodule");
